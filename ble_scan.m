% funkcja skanowania
function [devices] = ble_scan()

    devices_list = blelist();
    
    % Obsluga braku urzadzen
    if size(devices_list) <= 0
       devices = -1; 
    else
        devices = devices_list(:,1:end-1);
    end
    

%     dev1 = '20-AA-00-21-37-11';
%     dev2 = '20-AA-00-42-00-12';
%     
%     devices = [dev1; dev2];
end 