% funkcja odczytu poziomu baterii
function [bat_lvl] = ble_read(dev)
    
    ble_char = characteristic(dev, 0x180D, 0x2A37);
    try
        tmp = read(ble_char);
    catch
        tmp = -1;
    end
    
    bat_lvl = tmp(1);

    %     bat_lvl = 50;
end